const express = require('express')
const nodemailer = require('nodemailer')
const validator = require('validator')
const xssFilters = require('xss-filters')
const app = express()

app.use(express.json())

app.get('/', function (req, res) {
  res.status(405).json({ error: 'sorry!' })
})

app.post('/', function (req, res) {
  const attributes = ['name', 'phone']
  const sanitizedAttributes = attributes.map(n => validateAndSanitize(n, req.body[n]))
  const someInvalid = sanitizedAttributes.some(r => !r)

  if (someInvalid) {
    return res.status(422).json({ error: 'Не верно заполнены поля' })
  }

  sendMail(...sanitizedAttributes)
  res.status(200).json({ message: 'Успех!' })
})

module.exports = {
  path: '/api/contact',
  handler: app
}

const validateAndSanitize = (key, value) => {
  const rejectFunctions = {
    name: v => v.length < 4,
    phone: v => !validator
  }
  // If object has key and function returns false, return sanitized input. Else, return false
  // eslint-disable-next-line no-prototype-builtins
  return rejectFunctions.hasOwnProperty(key) && !rejectFunctions[key](value) && xssFilters.inHTMLData(value)
}

const sendMail = (name, phone) => {
  const transporter = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    secure: process.env.SMTP_SECURE,
    auth: {
      user: process.env.SMTP_MAIL,
      pass: process.env.SMTP_PASSWORD
    }
  })
  transporter.sendMail({
    from: process.env.SMTP_MAIL,
    to: process.env.SMTP_TO,
    subject: process.env.SMTP_SUBJECT,
    text: 'Новая заявка с сайта' + process.env.APP,
    html: '<p><b>Имя:</b> ' + name + '</p><p><b>Номер телефона:</b> <a href="tel:' + phone + '" title="' + name + '">' + phone + '</a></p>'
  })
  // system
  // nodemailer.createTransport({
  //   sendmail: true,
  //   newline: 'unix',
  //   path: '/usr/sbin/sendmail'
  // })
}
